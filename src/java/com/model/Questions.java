/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import java.io.Serializable;

/**
 *
 * @author VY
 */
public class Questions implements Serializable {

    private int id;
    private int number;
    private String content;
    private Exams ex_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Exams getEx_id() {
        return ex_id;
    }

    public void setEx_id(Exams ex_id) {
        this.ex_id = ex_id;
    }

    public Questions(int id, int number, String content, Exams ex_id) {
        this.id = id;
        this.number = number;
        this.content = content;
        this.ex_id = ex_id;
    }

    public Questions() {
    }
}
