/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import java.io.Serializable;

/**
 *
 * @author VY
 */
public class Exams implements Serializable {

    private int id;
    private String name;
    private int quest;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuest() {
        return quest;
    }

    public void setQuest(int quest) {
        this.quest = quest;
    }

    public Exams() {
    }

    public Exams(int id, String name, int quest) {
        this.id = id;
        this.name = name;
        this.quest = quest;
    }

}
