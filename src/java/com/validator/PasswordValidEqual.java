/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author VY
 */
@FacesValidator(value = "PasswordValidEqual")
public class PasswordValidEqual implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        String pass_1 = (String) o;
        UIInput confirmComponent = (UIInput) uic.getAttributes().get("password2");
        String pass_2 = (String) confirmComponent.getSubmittedValue();
        if (!pass_1.equals(pass_2)) {
            confirmComponent.setValid(false);
            FacesMessage msg = new FacesMessage("Mật khẩu không trùng nhau!");
            throw new ValidatorException(msg);
        }
    }
}
