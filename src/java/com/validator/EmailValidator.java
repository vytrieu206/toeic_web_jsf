/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author VY
 */
@FacesValidator(value = "EmailValidator")
public class EmailValidator implements Validator {

    private static final String EMAIL_REGEX = "(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$)";

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        Pattern pattern = Pattern.compile(EMAIL_REGEX);
        String email = ((String) o).trim();
        Matcher match = pattern.matcher(email);
        if (!match.matches()) {
            FacesMessage msg = new FacesMessage("Email không hợp lệ!");
            throw new ValidatorException(msg);
        }
    }
}
