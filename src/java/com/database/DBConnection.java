/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.database;

import com.mysql.jdbc.Driver;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author VY
 */
public class DBConnection implements Serializable {

    private String db_url = "jdbc:mysql://localhost:3306/toeic";
    private String db_user = "root";
    private String db_pass = "vy06201996";

    private Connection cn = null;
    protected PreparedStatement ps = null;
    protected ResultSet rs = null;

    public Connection connect() throws SQLException {
        DriverManager.registerDriver(new Driver());
        cn = DriverManager.getConnection(db_url, db_user, db_pass);
        return cn;
    }

    public void disconnect() {
        try {
            cn.close();
        } catch (SQLException ex) {
            System.err.println(ex);
        }
    }
}
