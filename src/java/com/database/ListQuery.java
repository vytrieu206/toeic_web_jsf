/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.database;

import com.model.Exams;
import com.model.Users;
import com.sun.jndi.cosnaming.CNCtx;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author VY
 */
public class ListQuery extends DBConnection implements Serializable {

    public List<Users> getUsers() {
        List<Users> list = new ArrayList<Users>();
        String sql = "SELECT * FROM user";
        try {
            ps = connect().prepareStatement(sql);
            Users us;
            rs = ps.executeQuery();
            while (rs.next()) {
                us = new Users(rs.getInt("Us_ID"), rs.getString("Us_Email"), rs.getString("Us_Pass"), rs.getString("Us_Role"));
                list.add(us);
            }
            disconnect();
            return list;
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }

    public int addUser(Users us) {
        String sql = "INSERT INTO user (Us_Email, Us_Pass, Us_Role) VALUES (?,?,?)";
        int result = 0;
        try {
            ps = connect().prepareStatement(sql);
            ps.setString(1, us.getEmail());
            ps.setString(2, us.getPass());
            ps.setString(3, us.getRole());

            result = ps.executeUpdate();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        disconnect();
        return result;
    }

    public List<Exams> getExams() {
        List<Exams> list = new ArrayList<>();
        String sql = "SELECT * FROM exams";
        try {
            ps = connect().prepareStatement(sql);
            rs = ps.executeQuery();
            Exams exams;
            while (rs.next()) {
                exams = new Exams(rs.getInt("Ex_ID"), rs.getString("Ex_Name"), rs.getInt("Ex_Quest"));
                list.add(exams);
            }
            disconnect();
            return list;
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }

    public Exams getEx_Item(int id) {
        String sql = String.format("SELECT * FROM exams WHERE Ex_ID = %d", id);
        System.out.println(sql);
        try {
            ps = connect().prepareStatement(sql);
            rs = ps.executeQuery();
            Exams ex = null;
            while (rs.next()) {
                ex = new Exams(rs.getInt("Ex_ID"), rs.getString("Ex_Name"), rs.getInt("Ex_Quest"));
            }
            return ex;
        } catch (SQLException ex1) {
            System.err.println(ex1.getMessage());
            return null;
        }
    }
}
