/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manageBean;

import com.database.ListQuery;
import com.model.Exams;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author VY
 */
@ManagedBean(name = "HomeContentBean")
@SessionScoped
public class HomeContentBean implements Serializable {

    private ListQuery query;
    private String message;
    private List<Exams> list_ex = new ArrayList<>();

    public List<Exams> getList_ex() {
        query = new ListQuery();
        list_ex = query.getExams();
        if (list_ex == null) {
            this.message = "Chưa có dữ liệu!";
        }
        return list_ex;
    }

    public void setList_ex(List<Exams> list_ex) {
        this.list_ex = list_ex;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String gotoExam() {
        return "exam";
    }
}
