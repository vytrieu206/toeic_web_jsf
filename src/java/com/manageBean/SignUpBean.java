/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manageBean;

import com.database.ListQuery;
import com.model.Users;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author VY
 */
@ManagedBean(name = "SignUpBean")
public class SignUpBean implements Serializable {

    private String email;
    private String pass_1;
    private String pass_2;
    private String message;
    private List<Users> us_list = new ArrayList<>();

    public String getPass_2() {
        return pass_2;
    }

    public void setPass_2(String pass_2) {
        this.pass_2 = pass_2;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass_1() {
        return pass_1;
    }

    public void setPass_1(String pass_1) {
        this.pass_1 = pass_1;
    }

    private boolean emailNotExist() {
        ListQuery query = new ListQuery();
        us_list = query.getUsers();
        if (us_list != null) {
            for (Users u : us_list) {
                if (u.getEmail().equalsIgnoreCase(this.email)) {
                    this.message = "Email đã có!";
                    return false;
                } else {
                    if (!this.pass_1.equals(this.pass_2)) {
                        this.message = "Mật khẩu không khớp!";
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public String signup() {
        boolean valid = emailNotExist();
        if (valid == true) {
            ListQuery query = new ListQuery();
            Users us = new Users();
            us.setEmail(this.email);
            us.setPass(this.pass_1);
            us.setRole("user");

            int rst = query.addUser(us);
            if (rst == 1) {
                return "home";
            } else {
                this.message = "Lỗi đăng ký";
                return "signup";
            }
        } else {
            return "signup";
        }
    }
}
