/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manageBean;

import com.database.ListQuery;
import com.model.Exams;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author VY
 */
@ManagedBean(name = "ExamBean")
@SessionScoped
public class ExamBean implements Serializable {

    private String name;
    private int id;
    private int quest;
    private String time;
    private ListQuery query = new ListQuery();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        Exams ex = new Exams();
        ex = query.getEx_Item(this.id);
        return ex.getName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuest() {
        Exams ex = new Exams();
        ex = query.getEx_Item(this.id);
        return ex.getQuest();
    }

    public void setQuest(int quest) {
        this.quest = quest;
    }

    public String getTime() {
        return "14/4/2018";
    }

    public void setTime(String time) {
        this.time = time;
    }
}
