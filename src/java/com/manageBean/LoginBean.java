/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manageBean;

import com.database.ListQuery;
import com.model.Users;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author VY
 */
@ManagedBean(name = "LoginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private String ip_us;
    private String ip_pass;
    private String message;
    private List<Users> us_list = new ArrayList<>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIp_us() {
        return ip_us;
    }

    public void setIp_us(String ip_us) {
        this.ip_us = ip_us;
    }

    public String getIp_pass() {
        return ip_pass;
    }

    public void setIp_pass(String ip_pass) {
        this.ip_pass = ip_pass;
    }

    private boolean getUsers() {
        boolean valid = false;
        ListQuery query = new ListQuery();
        us_list = query.getUsers();
        if (us_list != null) {
            for (Users u : us_list) {
                if (u.getEmail().equals(this.ip_us) && u.getPass().equals(this.ip_pass)) {
                    valid = true;
                }
            }
        }
        return valid;
    }

    public String login() {
        boolean valid = getUsers();
        if (valid == true) {
            return "home";
        } else {
            this.message = "Tài khoản không hợp lệ!";
            return "login";
        }
    }
}
